from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from .forms import StudentForm
from .models import Student


class StudentListView(ListView):
    model = Student
    template_name = "students/students_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        students = Student.objects.all()
        context["students"] = students
        return context


class CreateStudentView(CreateView):
    model = Student
    template_name = "students/students_create.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:all_students")


class UpdateStudentView(UpdateView):
    model = Student
    form_class = StudentForm
    template_name = "students/update_students.html"
    success_url = reverse_lazy("students:all_students")


class DeleteStudentView(DeleteView):
    model = Student
    success_url = reverse_lazy("students:all_students")
