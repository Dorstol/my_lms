from uuid import uuid4

from django.db import models

from app.models import Person
from groups.models import Group
from .validators import file_size, file_extension


class Student(Person):
    uuid = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid4,
        unique=True,
        db_index=True
    )
    avatar = models.ImageField(upload_to="media/students/avatar", null=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name="students")
    grade = models.PositiveSmallIntegerField(default=0, null=True)
    resume = models.FileField(upload_to='media/docs',
                              null=True,
                              validators=[file_size, file_extension])

    def __str__(self):
        return f'{self.first_name} {self.last_name} {self.email}'
