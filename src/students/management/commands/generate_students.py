from django.core.management.base import BaseCommand
from faker import Faker


fake = Faker()


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, default=10)

    def handle(self, *args, **options):
        count = options['count']
        for i in range(count):
            name = fake.name()
            email = fake.email()
            self.stdout.write(f'Creating user {name} with email {email}\n')
