from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UsernameField
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _

from app.models import Profile
from app.utils.validators import email_validator


class LoginForm(AuthenticationForm):
    username = UsernameField(label=_("Input email or your phone"), validators=[email_validator])

    class Meta:
        model = get_user_model()
        fields = ("phone", "password1")


class RegistrationForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ("phone", "email", "password1", "password2")


class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ("photo", "location", "gender", "biography", "type")
