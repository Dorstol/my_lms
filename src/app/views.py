from django.contrib.auth import get_user_model, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, \
    PasswordResetView
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import TemplateView, CreateView, RedirectView
from webargs import fields
from webargs.djangoparser import use_kwargs

from app.forms import RegistrationForm, LoginForm
from app.services.emails import send_registration_email
from app.utils.token_generator import TokenGenerator
from students.models import Student


class IndexBasedView(TemplateView):
    template_name = 'basic/index.html'


def handle404(request, exception=None):
    return render(request, "basic/404.html", {})


def custom_permission_denied_view(request, exception=None):
    return render(request, "basic/403.html", {})


@use_kwargs({
    'search': fields.Str(missing=None)
},
    location="query"
)
def searchbar(request, search):
    if request.method == 'GET':
        post = Student.objects.filter(
            Q(pk__contains=search) | Q(first_name__contains=search) | Q(last_name__contains=search) | Q(
                email__contains=search)
        )
    else:
        post = None
    return render(request, 'basic/searchbar.html', {"post": post})


class Login(LoginView):
    success_url = reverse_lazy('app:index')
    form_class = LoginForm


class Logout(LogoutView):
    pass


class Registration(CreateView):
    template_name = "registration/create_user.html"
    form_class = RegistrationForm
    success_url = reverse_lazy('app:index')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()

        send_registration_email(
            request=self.request, user_instance=self.object
        )

        return super().form_valid(form)


class ActivateUser(RedirectView):
    url = reverse_lazy("app:index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return HttpResponse("Wrong data")
        if current_user and TokenGenerator().check_token(current_user, token):
            if current_user.is_active:
                render(request, "basic/index.html", {"message": "You are already activated!"})
            current_user.is_active = True
            current_user.save()

            login(request, current_user, backend='django.contrib.auth.backends.ModelBackend')
            return super().get(request, *args, **kwargs)
        return HttpResponse("Wrong data")


class ResetPasswordView(PasswordResetView, SuccessMessageMixin):
    success_message = "We've emailed you instructions for setting your password, " \
                      "if an account exists with the email you entered. You should receive them shortly." \
                      " If you don't receive an email, " \
                      "please make sure you've entered the address you registered with, and check your spam folder."
    success_url = reverse_lazy('app:index')


class ProfileView(TemplateView, LoginRequiredMixin):
    template_name = "profile/profile.html"
