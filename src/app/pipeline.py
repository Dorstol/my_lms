def cleanup_social_account(backend, uid, user=None, *args, **kwargs):
    if backend == 'github':
        user.avatar = kwargs['response']['avatar_url']
        user.save()

    return {"user": user}
