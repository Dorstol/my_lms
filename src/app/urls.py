from django.contrib.auth import views as auth_views
from django.urls import path

from app.views import IndexBasedView, searchbar, Login, Logout, Registration, \
    ResetPasswordView, ActivateUser, ProfileView

app_name = "app"

urlpatterns = [
    path('', IndexBasedView.as_view(), name='index'),
    path("search/", searchbar, name="searchbar"),
    path("login/", Login.as_view(), name="login"),
    path("logout/", Logout.as_view(), name="logout"),
    path("signup/", Registration.as_view(), name="registration"),
    path("activate/<str:uuid64>/<str:token>/", ActivateUser.as_view(), name="activate_user"),
    path(
        "password-reset/",
        ResetPasswordView.as_view(),
        name="password_reset"
    ),
    path(
        'password-reset-confirm/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm'
    ),
    path(
        'password-reset-complete/',
        auth_views.PasswordResetCompleteView.as_view(template_name='registration/password_reset_complete.html'),
        name='password_reset_complete'
    ),
    path('profile/', ProfileView.as_view(), name="profile"),

]
