from datetime import datetime

from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.mail import send_mail
from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

from app.managers import CustomerManager


class Person(models.Model):
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    email = models.EmailField(max_length=254, unique=True)
    birthday = models.DateField(null=True)

    def age(self):
        birthday = datetime.strptime(str(self.birthday), '%Y-%m-%d').date()
        current_date = datetime.today().date()
        result = current_date - birthday
        return f'{result.days // 365}, {(result.days % 365) // 30} years old'

    class Meta:
        abstract = True


class Customers(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_("First name"), max_length=150, blank=True)
    last_name = models.CharField(_("Last name"), max_length=150, blank=True)
    email = models.EmailField(_("Email address"), null=True, blank=True)
    phone = PhoneNumberField(_("Phone number"), null=True, blank=True,
                             validators=[RegexValidator(r'^\d{3}-\d{3}-\d{4}$')])
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    avatar = models.ImageField(upload_to='media/img/profiles/', null=True)

    objects = CustomerManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def __str__(self):
        return str(self.phone)

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)


class Profile(models.Model):
    class Gender(models.TextChoices):
        MALE = "Male", _("Male")
        FEMALE = "Female", _("Female")
        ALIEN = "Alien", _("Alien")

    class UserType(models.TextChoices):
        STUDENT = "Student", _("Student")
        MENTOR = "Mentor", _("Mentor")
        TEACHER = "Teacher", _("Teacher")

    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, related_name="profile")
    photo = models.ImageField(upload_to="media/img/profiles/", blank=True)
    location = models.CharField(max_length=50, blank=True)
    gender = models.CharField(max_length=10, choices=Gender.choices, blank=True, default=Gender.ALIEN)
    biography = models.TextField(blank=True)
    type = models.CharField(max_length=10, choices=UserType.choices, blank=True, default=UserType.STUDENT)
