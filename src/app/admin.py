from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from app.models import Customers
from groups.models import Group
from students.models import Student
from teachers.models import Teacher


@admin.register(Customers)
class UserAdmin(admin.ModelAdmin):
    icon_name = 'person'
    list_display = ["first_name", "last_name", "email", "phone", "is_staff"]
    list_filter = ["is_staff"]
    ordering = ("first_name", "last_name")
    empty_value_display = '-empty-'


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    date_hierarchy = "birthday"
    list_display = ["first_name", "last_name", "email", "group", "grade", "resume", "age"]
    list_filter = ["grade", "group"]


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ("full_name", "email", "phone", "address", "group_count", "group_links")
    fieldsets = (
        (
            "Personal info", {
                "fields": ("full_name", "email", "phone",)
            }
        ),
        (
            "Additional info", {
                "fields": ("address", "group",)
            }
        )
    )

    def group_count(self, obj):
        if obj.group:
            return obj.group.count()
        else:
            return 0

    def group_links(self, obj):
        if obj.group:
            groups = obj.group.all()
            links = []
            for group in groups:
                links.append(
                    f"<a class='button' href='{reverse('admin:groups_group_change', args=[group.pk])}'>{group.name}</a>"
                )
            return format_html("</br>".join(links))
        return "No groups"


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ("name", "description", "about", "created_at",)
    ordering = ("name",)
