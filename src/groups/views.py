from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from .forms import GroupForm
from .models import Group


class ListOfGroupView(ListView):
    model = Group
    template_name = "groups/get_all_groups.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        groups = Group.objects.all()
        context["groups"] = groups
        return context


class CreateGroupView(CreateView):
    model = Group
    template_name = "groups/create_group.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:get_all_groups")


class UpdateGroupView(UpdateView):
    model = Group
    template_name = "groups/update_group.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:get_all_groups")


class DeleteGroupView(DeleteView):
    model = Group
    success_url = reverse_lazy("groups:get_all_groups")
