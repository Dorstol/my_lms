from django.urls import path
from django.conf.urls.static import static
from django.conf.urls.static import settings
from .views import ListOfGroupView, CreateGroupView, UpdateGroupView, DeleteGroupView

app_name = "groups"

urlpatterns = [
    path('', ListOfGroupView.as_view(), name='get_all_groups'),
    path('create/', CreateGroupView.as_view(), name='create_group'),
    path("update/<int:pk>/", UpdateGroupView.as_view(), name="update_group"),
    path("delete/<int:pk>/", DeleteGroupView.as_view(), name="delete_group"),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
