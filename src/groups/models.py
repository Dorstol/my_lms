from datetime import datetime

from django.db import models


class Group(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    about = models.TextField(blank=True)
    logo = models.ImageField(upload_to='static/img', blank=True, null=True)
    created_at = models.DateTimeField(default=datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.name
