from datetime import datetime

from django.db import models

from groups.models import Group


class Teacher(models.Model):
    avatar = models.ImageField(upload_to="static/img", null=True)
    full_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    phone = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    created_at = models.DateTimeField(default=datetime.now)
    updated_at = models.DateTimeField(default=datetime.now, null=True)
    group = models.ManyToManyField(Group, related_name="teachers")

    def __str__(self):
        return self.full_name
