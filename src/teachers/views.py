from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from .forms import TeacherForm
from .models import Teacher


class TeacherListView(ListView):
    model = Teacher
    template_name = "teachers/teachers_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        teachers = Teacher.objects.all()
        context["teachers"] = teachers
        return context


class CreateTeacherView(CreateView):
    model = Teacher
    template_name = "teachers/create_teachers.html"
    form_class = TeacherForm
    success_url = reverse_lazy('teachers:get_all_teachers')


class UpdateTeacherView(UpdateView):
    model = Teacher
    template_name = "teachers/update_teacher.html"
    form_class = TeacherForm
    success_url = reverse_lazy('teachers:get_all_teachers')


class DeleteTeacherView(DeleteView):
    model = Teacher
    success_url = reverse_lazy('teachers:get_all_teachers')
