from django.core.exceptions import ValidationError
from django.forms import ModelForm

from .models import Teacher


class TeacherForm(ModelForm):
    class Meta:
        model = Teacher
        fields = ["avatar", "full_name", "email", "phone", "address", "group"]

    @staticmethod
    def normalize_text(text: str) -> str:
        return text.strip().capitalize()

    def clean_email(self):
        email = self.cleaned_data["email"]

        if "@yandex" in email.lower():
            raise ValidationError("Yandex domain is forbidden in our country")

        return email

    def clean_full_name(self):
        return self.normalize_text(self.cleaned_data["full_name"])

    def clean_address(self):
        return self.normalize_text(self.cleaned_data["address"])
