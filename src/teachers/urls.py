from django.urls import path
from .views import TeacherListView, CreateTeacherView, UpdateTeacherView, DeleteTeacherView


app_name = "teachers"

urlpatterns = [
    path('', TeacherListView.as_view(), name='get_all_teachers'),
    path('create/', CreateTeacherView.as_view(), name='create_teacher'),
    path("update/<int:pk>/", UpdateTeacherView.as_view(), name="update_teacher"),
    path("delete/<int:pk>/", DeleteTeacherView.as_view(), name="delete_teacher"),
]
