from django.conf.urls.static import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('', include('app.urls')),
    path('admin/', admin.site.urls),
    path('students/', include('students.urls')),
    path('teachers/', include('teachers.urls')),
    path('groups/', include('groups.urls')),
    path('oauth/', include('social_django.urls', namespace="social")),
    path('accounts/', include('phone_auth.urls', namespace="phone_auth")),
    path('__debug__/', include('debug_toolbar.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
handler404 = "app.views.handle404"
handler403 = 'app.views.custom_permission_denied_view'
